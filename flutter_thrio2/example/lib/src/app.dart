import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:thrio/thrio.dart';
import 'module.dart';

void main() => runApp(const MainApp());
void biz1() => runApp(const MainApp(entrypoint: 'biz1'));
void biz2() => runApp(const MainApp(entrypoint: 'biz2'));

class MainApp extends StatefulWidget {
  const MainApp({Key key, String entrypoint = 'main'})
      : _entrypoint = entrypoint,
        super(key: key);

  final String _entrypoint;

  @override
  _MainAppState createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  @override
  void initState() {
    super.initState();

    ThrioModule.init(Module(), widget._entrypoint);
  }

  @override
  Widget build(BuildContext context) => ExcludeSemantics(
        child: NavigatorMaterialApp(
          localizationsDelegates: const[
            S.delegate, //应用程序的翻译回调
            GlobalMaterialLocalizations.delegate, //material 组件的翻译回调
            GlobalCupertinoLocalizations.delegate, //普通 widget 的翻译回调
            GlobalWidgetsLocalizations.delegate
          ],

          //设置支持的语系，将 en 设置为第一项
          supportedLocales: [const Locale('en', ''), ...S.delegate.supportedLocales],
          
          //title 的国际化回调
          onGenerateTitle: (context){
            return S.of(context).app_title;
          },

          home: const NavigatorHome(showRestartButton: true),
          builder: (context, child) => Container(
            child: child,
          ),
          theme: ThemeData(
            pageTransitionsTheme: const PageTransitionsTheme(builders: {
              TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
              TargetPlatform.android: CupertinoPageTransitionsBuilder(),
            }),
          ),
        ),
      );
}
