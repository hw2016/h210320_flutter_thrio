import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:convert';

import 'package:thrio_example/src/models/teacher.dart';

class Student {
  String id;
  String name;
  int score;
  Teacher teacher;

  //构造函数
  Student({this.id, this.name, this.score, this.teacher});

  //JSON 解析工厂类
  factory Student.fromJson(Map<String, dynamic> parsedJson){
    return Student(
        id: parsedJson['id'] as String,
        name : parsedJson['name'] as String,
        score : parsedJson ['score'] as int,

        // 增加映射规则
        teacher: Teacher.fromJson(parsedJson['teacher'] as Map<String, dynamic>)
    );
  }
}
