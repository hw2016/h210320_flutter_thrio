/**
 * create by hw 2021-3-23
 */

class HomeDeviceModel {
  String deviceIcon;
  String deviceName;
  String deviceId;

  //在线状态 
  String onlineStatus; //todo 须将 int 值转为 string
  //绑定状态
  String bindStatus; //todo 须将 int 值转为 string

  //定义构造函数
  HomeDeviceModel({
    this.deviceIcon,
    this.deviceName,
    this.deviceId,
    this.onlineStatus,
    this.bindStatus
  });
}