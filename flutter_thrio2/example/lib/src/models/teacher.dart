import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:convert';

class Teacher {
  String name;
  int age;
  Teacher({this.name,this.age});

  factory Teacher.fromJson(Map<String, dynamic> parsedJson){
    return Teacher(
        name : parsedJson['name'] as String,
        age : parsedJson ['age'] as int
    );
  }
}
