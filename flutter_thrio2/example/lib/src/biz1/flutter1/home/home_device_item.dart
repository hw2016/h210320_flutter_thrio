/**
 * create by hw 2021-3-23
 */

import 'package:flutter/material.dart';
import 'package:thrio_example/src/models/home_device_model.dart';
import 'package:thrio/thrio.dart';

class HomeDeviceItem extends StatelessWidget {

  final HomeDeviceModel model; 

  const HomeDeviceItem({Key key, this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    return Container(
      child: buildItem(context),
    );
  }

  // 配置列表 item
  Widget buildItem(BuildContext context) {
    double itemHeight = 60.0;

    return Container( margin: EdgeInsets.only(left: 10, right: 10, top: 10),
       
      //设置边框
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(6.0)), //设置圆角
      ),
      child:
        new Row(
        children: <Widget>[

          new Expanded(
            child: new Row(                                      
              children: <Widget>[

                //设备图标   
                Image.asset(model.deviceIcon, width: itemHeight, height: itemHeight,),
                
                Expanded(                   
                  child: Column(                    
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      //设备标题
                      Text(model.deviceName, maxLines: 1, overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 16, color: Color(0xff333333)),),
                      
                      //添加 设备标题 和 在线状态 之间的间距
                      Padding(padding: EdgeInsets.only(bottom: 8)),

                      Row(      
                        // crossAxisAlignment: CrossAxisAlignment.center,                                   
                        children: <Widget>[                                                   
                          //Image.asset('assets/images/icon_home_point_green.png', width: 30, height: 30,),              
                           
                          //设备在线状态
                          Text(model.onlineStatus, maxLines: 1, style: TextStyle(fontSize: 12, color: Color(0xff2ecc71)),),
                          
                          //设备的绑定状态
                          Text(model.bindStatus, maxLines: 1, style: TextStyle(fontSize: 12, color: Colors.grey),)
                        ],
                      ),
                    ],
                  ),
                 ),

              ],
            ),
          ),
          
          Row( 
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
                 
              InkWell(              
                onTap: () => ThrioNavigator.push(
                  url: '/biz2/flutter2',
                  params: {
                    '1': {'2': '3'}
                  },
                ),
                //播放图标 
                child: Image.asset('assets/images/icon_home_play_black.png', width: 60, height: 60,),                                         
              ),        
 
              InkWell(              
                onTap: () => ThrioNavigator.push(
                  url: '/biz1/flutter3',
                  params: {
                    '1': {'2': '3'}
                  },
                ),
                //设置图标
                child: Image.asset('assets/images/icon_home_setting_black.png', width: 60, height: 60,),                                         
              ), 
               
            ],
          ),
        ],
      )

    );
  }
}

