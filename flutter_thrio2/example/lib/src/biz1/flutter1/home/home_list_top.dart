/**
 * create by hw 2021-3-23
 */

import 'package:flutter/material.dart';
import 'package:thrio/thrio.dart';

class HomeListTopWidget extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {

    //定义家庭/添加按钮的宽高
    double topBtnWH = 26.0;

    return Column(  
      children: <Widget>[
        
        Padding(padding: EdgeInsets.only(top: 15)),

        // 家庭、添加按钮
        Row(
 
          children: <Widget>[ 
            Padding(padding: EdgeInsets.only(left: 15)),

             Expanded(
                child: Row(
                  children: [                    
                    ClipRRect(               
                      //家庭图标 
                      child: Image.asset('assets/images/icon_home_family.png', width: topBtnWH, height: topBtnWH,),
                    ),

                    //添加图标和文本之间的间距
                    Padding(padding: EdgeInsets.only(right: 8)),

                    //家庭名称
                    Text('我的家', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.black), maxLines: 1,),
                  ],
                ),
            ),

            InkWell(              
              onTap: () => ThrioNavigator.push(
                url: '/biz1/flutter3',
                params: {
                  '1': {'2': '3'}
                },
              ),
            
              child: 
                  //添加设备图标
                  Row(children: [                   
                    Image.asset('assets/images/icon_home_add.png', width: topBtnWH, height: topBtnWH,),  
                    Padding(padding: EdgeInsets.only(right: 15)),
                  ],)
                                         
            ),            
          ],
        ),

        //添加间距
        Padding(padding: EdgeInsets.only(top: 15)),

        //设备数量
        Row(
          children: <Widget>[
            Padding(padding: EdgeInsets.fromLTRB(15, 0, 0, 1)),
            Text('所有设备(10)', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),)
          ],
        )        
      ], 
    );
  }
}