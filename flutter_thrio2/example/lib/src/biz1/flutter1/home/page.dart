/**
 * create by hw 2021-3-23
 */

import 'dart:convert';
import 'dart:io' show HttpClient, Platform;
import 'dart:math';

import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:thrio/thrio.dart';
import 'package:thrio_example/src/biz1/flutter1/home/home_device_item.dart';
import 'package:thrio_example/src/models/home_device_model.dart';

import '../../../models/people.dart';
import 'home_list_top.dart';

class Page extends StatefulWidget {
  const Page({
    Key key,
    this.moduleContext,
    this.index,
    this.params,
  }) : super(key: key);

  final int index;

  final ModuleContext moduleContext;

  final dynamic params;

  @override
  _PageState createState() => _PageState();
}

class _PageState extends State<Page> {
  TextEditingController _inputController;
  @override
  void initState() {
    super.initState();

    if (mounted) {
      _inputController = TextEditingController();
    }
  }

  @override
  void dispose() {
    _inputController.dispose();
    super.dispose();
  }
 
  @override
  Widget build(BuildContext context) {
 
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        body: TabBarView(
          children: [
            HomeWidget(),
            PhotoWidget(),
            MineWidget()
          ],
        ),
        bottomNavigationBar: TabBar(
          tabs: [
            Tab(icon: Icon(Icons.home), text: '首页',),
            Tab(icon: Icon(Icons.phone), text: '相册',),
            Tab(icon: Icon(Icons.mediation), text: '我的',),
          ],
          unselectedLabelColor: Colors.blueGrey, //未选中的 tab 的颜色
          labelColor: Colors.blue, //已选中的 tab 的颜色
          indicator: const BoxDecoration(), //取消下划线
        ),
      ),
    );
  }
}

class HomeWidget extends StatelessWidget {

  //生成数据模型
  var model = HomeDeviceModel(
    deviceIcon: 'assets/images/icon_home_door_lock.png', 
    deviceName: '张三的智能门锁1-张三的智能门锁2-张三的智能门锁3-张三的智能门锁4',
    deviceId: '1001',
    onlineStatus: '在线',
    bindStatus: '    已绑定'
  );

  @override
  Widget build(BuildContext context) {
    
    return Container(
      color: Color(0xfff4f4f4), //设置列表的背景颜色
      //设备列表
      child: ListView.builder(
        itemCount: 10,
        itemExtent: 75,
        itemBuilder: (BuildContext content, int index) {
           
          if (index == 0) { //在顶部设置 header
            return HomeListTopWidget();
          } else {
            return HomeDeviceItem(model: model,);
          }
        },
      ),
    );
  }
}

class PhotoWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    return CustomScrollView();
  }
}

class MineWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    return CustomScrollView();
  }
}
